"A little orchard by the dwelling" Taras Shevchenko

A little orchard by the dwelling,

The June bugs hum above its top;

Home strolling girls sing, laugh, and hop;

The ploughmen from the fields are coming, 

While mothers wait for them to sup.

The kin are eating by the dwelling;

The evening star peeks o`er a bough;

A daughter serves with knitted brow

While how to serve the mother`s telling, 

But nightingale just won`t allow.

Beside the dwelling placed the mother

Her little children in their nest, 

And with a dream herself in blest.

All`s still. � The girls and warblers, only, 

Seem to forget it`s time of rest.

Translated by Waldimir Semenyna
